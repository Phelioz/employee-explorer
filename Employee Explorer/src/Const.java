
public class Const {

	// Table Name
	static final String EMP_TABLE = "employees";
	static final String DEP_TABLE = "departments";
	
	// Employees Columns
	static final String EMP = "employee_id";
	static final String FIRST = "first_name";
	static final String LAST = "last_name";
	static final String EMAIL = "email";
	static final String PHONE = "phone_number";
	static final String HIRE = "hire_date";
	static final String JOB = "job_id";
	static final String SALARY = "salary";
	static final String COMISS = "commision_pct";
	static final String MANAGER = "manager_id";
	static final String DEPAR = "department_id";
	static final String DEPAR_NAME = "department_name";
	
	// Prepared Statements Strings
	static final String firstSelectFirstLetter = "SELECT " + EMP + ", " + FIRST + ", " + LAST + " FROM " + EMP_TABLE + " "
			+ "WHERE " + FIRST + " LIKE '";
	
	static final String lastSelectFirstLetter = "SELECT " + EMP + ", " + FIRST + ", " + LAST + " FROM " + EMP_TABLE + " "
			+ "WHERE " + LAST + " LIKE '";
	
	static final String ALL_ABOUT_EMPLOYEE = "SELECT * FROM " + EMP_TABLE + " "
			+ "WHERE " + EMP + "='";
	
	static final String ALL_ABOUT_EMPLOYEE_WHERE = "SELECT * FROM " + EMP_TABLE + " "
			+ "WHERE " + EMP + "='";
	
	static final String ALL_DEPARTMENTS = "SELECT " + DEPAR + "," + DEPAR_NAME + " FROM " + DEP_TABLE
			+ " WHERE " + DEPAR + " IS NOT NULL"
			+ " GROUP BY " + DEPAR;
	
	static final String ALL_MANAGER = "SELECT " + EMP + ", " + FIRST + ", " + LAST + " FROM " + EMP_TABLE + " "
			+ "WHERE " + EMP + " IN (SELECT manager_id FROM employees)";
	
	static final String FIRST_LAST_WHERE_DEP = "SELECT " + EMP + ", " + FIRST + ", " + LAST + " FROM " + EMP_TABLE + " "
			+ "WHERE " + DEPAR + "=";
	
	static final String FIRST_LAST_WHERE_MAN =  "SELECT " + EMP + ", " + FIRST + ", " + LAST + " FROM " + EMP_TABLE + " "
			+ "WHERE " + MANAGER + "=";
			
	
	
	// Update Statements Strings
	static final String UPDATE_EMPLOYEE_ID = "UPDATE " + EMP_TABLE + " "
			+ "SET " + EMP + "='";
	static final String UPDATE_EMPLOYEE_ID2 = "' "
			+ "WHERE " + EMP + "='";
	
	static final String UPDATE_FIRST = "UPDATE " + EMP_TABLE + " "
			+ "SET " + FIRST + "='";
	static final String UPDATE_FIRST2 = "' "
			+ "WHERE " + FIRST + "='";
	
}
