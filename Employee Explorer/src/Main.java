


public class Main {

	public static void main(String[] args){
		
		LookUp lookUp = new LookUp();
		
		lookUp.makeConnection();
		lookUp.lookUpEmployee();
		lookUp.closeConnection();
	}
}
