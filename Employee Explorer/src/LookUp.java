import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;



public class LookUp {
	
	Connection connection;
	ResultSet resSet;
	
	
	Statement stmt;
	Statement stmt2;
	Statement stmt3;
	
	Scanner inputScanner;
	String inputString;
	
	String tempID = "";
	String tempID2 = "";
	
	String connectString = "jdbc:mysql://localhost:3306/db_up1";
	String username = "root";
	String password = "password";
	
	String mainOpt = "";
	String whereOpt = "";
	String indOpt = "";
	
	ArrayList<String> tempArr;
	
	LookUpHelper lookUpHelper;
	
	
	public LookUp(){
		init();
	}

	public void init() {
	// Add something here to get done when the object initializes
	lookUpHelper = new LookUpHelper();
	}
	
	// Makes connection to database
	public void makeConnection() {
		try {
			connection = DriverManager.getConnection(connectString, username, password);
			stmt = (Statement) connection.createStatement();
			stmt2 = (Statement) connection.createStatement();
			stmt3 = (Statement) connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	// Main Menu which lets you choose what category you want to search by
	public void lookUpEmployee() {

			try {
				outerloop:
				while (true) {
					int indexInt = 0;
					System.out.println("Main Menu:");
					System.out.println("1. All employees");
					System.out.println("2. First name");
					System.out.println("3. Last name");
					System.out.println("4. Department ID");
					System.out.println("5. Manager ID\n");
					System.out.println("6. Exit\n");
					inputScanner = new Scanner(System.in);
					inputString = inputScanner.next();
					if (lookUpHelper.isInt(inputString) == false) continue;
					indexInt = Integer.parseInt(inputString);
					if (indexInt < 0 || indexInt > 6) continue; 
					switch (indexInt) {
						case 1:
							allEmployees();
							break;
						case 2:
							firstName();
							break;
						case 3:
							lastName();
							break;
						case 4:
							departmentID();
							break;
						case 5:
							managerID();
							break;
						case 6:
							break outerloop;
						default:
							return;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	// Shows all Employees
	private void allEmployees() throws SQLException{
			// Resets tempID2
			tempID2 = "";
			// Chooses all employees
			inputString = "%";
				chooseEmployee(resSet);
	}
	
	// Shows all employees who's first names first letter start with your input
	private void firstName() throws SQLException{
		// Resets tempID2
		tempID2 = "";
		// Checks if it's a number and just one number
		while (true) {
			System.out.print("Type first letter of wanted name: ");
			inputString = inputScanner.next();
			if (inputString.length() > 1) continue; 
			System.out.println("");
			if (lookUpHelper.isInt(inputString)) continue; 
			
			
			chooseEmployee(resSet);
			break;
			
		}
	}
	
	// Shows all employees who's last names first letter start with your input
	private void lastName() throws SQLException{
		// Resets tempID2
		tempID2 = "";
		// Checks if it's a number and just one number
		while (true) {
			System.out.print("Type first letter of wanted name: ");
			inputString = inputScanner.next();
			if (inputString.length() > 1) continue; 
			System.out.println("");
			if (lookUpHelper.isInt(inputString)) continue; 
			
			
			chooseEmployeeLast(resSet);
			break;
			
		}
	}
	
	// Shows all department and lets you choose one to see all of it's employees
	private void departmentID() throws SQLException{
		resSet = stmt.executeQuery(Const.ALL_DEPARTMENTS);
		tempArr = lookUpHelper.resToArr(resSet);
		// Shows all departments and saves the departments IDs
		// in a array so you can select them
		int x = 0;
		boolean includes = false;
		while (true) {
	
			resSet = stmt.executeQuery(Const.ALL_DEPARTMENTS);
			tempArr = lookUpHelper.resToArr(resSet);
			
			System.out.println("");
			lookUpHelper.printTable(resSet);
			resSet.first();
			System.out.print("\nInsert index of department to see it's employees\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			System.out.println("");
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			
			if(inputString.equalsIgnoreCase("Back")) break;
			if (includes == true) {
				tempID = tempArr.get(x - 1);
				chooseEmployeeDepartment(resSet);
			}
			
		}
	}
	
	// Shows all managers and lets you choose one to see all of it's employees
	private void managerID() throws SQLException{
		resSet = stmt.executeQuery(Const.ALL_MANAGER);
		tempArr = lookUpHelper.resToArr(resSet);
		// Shows all departments and saves the departments IDs
		// in a array so you can select them
		int x = 0;
		boolean includes = false;
		while (true) {
	
			resSet = stmt.executeQuery("SELECT employee_id, first_name, last_name "
					+ "FROM employees "
					+ "WHERE employee_id IN(SELECT manager_id FROM employees)");
			tempArr = lookUpHelper.resToArr(resSet);
			
			System.out.println("");
			lookUpHelper.printTable(resSet);
			resSet.first();
			System.out.print("\nInsert index of manager to see the managers employees\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			System.out.println("");
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			
			if(inputString.equalsIgnoreCase("Back")) break;
			if (includes == true) {
				tempID = tempArr.get(x - 1);
				chooseEmployeeManager(resSet);
			}
			
		}
	}
	
	private void chooseEmployee(ResultSet resSet) throws SQLException {
		String firstLetter = inputString;
		resSet = stmt.executeQuery(Const.firstSelectFirstLetter + firstLetter + "%';");
		tempArr = lookUpHelper.resToArr(resSet);
		boolean includes;
		int x = 0;
		while (true) {
			//Resets tempID2 and resets the if input includes numeric value
			includes = false;
			tempID2 = "";
			resSet = stmt.executeQuery(Const.firstSelectFirstLetter + firstLetter + "%';");
			
			System.out.println("");
			lookUpHelper.printTable(resSet);
			resSet.first();
			System.out.print("\nInsert index to edit employee\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			System.out.println("");
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			
			if(inputString.equalsIgnoreCase("Back")) break;
			if (includes == true) {
				tempArr = lookUpHelper.resToArr(resSet);
				tempID = tempArr.get(x - 1);
				System.out.println(tempID);
				
				editEmployee(resSet);
			}
			
		}
	}
	
	private void chooseEmployeeLast(ResultSet resSet) throws SQLException {
		String firstLetter = inputString;
		resSet = stmt.executeQuery(Const.lastSelectFirstLetter + firstLetter + "%';");
		tempArr = lookUpHelper.resToArr(resSet);
		boolean includes;
		int x = 0;
		while (true) {
			//Resets tempID2 and resets the if input includes numeric value
			includes = false;
			tempID2 = "";
			
			resSet = stmt.executeQuery(Const.lastSelectFirstLetter + firstLetter + "%';");
			
			System.out.println("");
			lookUpHelper.printTable(resSet);
			resSet.first();
			System.out.print("\nInsert index to edit employee\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			System.out.println("");
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			
			if(inputString.equalsIgnoreCase("Back")) break;
			if (includes == true) {
				tempArr = lookUpHelper.resToArr(resSet);
				tempID = tempArr.get(x - 1);
				
				editEmployee(resSet);
			}
			
		}
	}

	private void chooseEmployeeDepartment(ResultSet resSet) throws SQLException {

		resSet = stmt.executeQuery(Const.FIRST_LAST_WHERE_DEP + tempID);
		boolean includes = false;
		
		int x = 0;

		while (true) {
		resSet = stmt3.executeQuery(Const.FIRST_LAST_WHERE_DEP + tempID);
		tempArr = lookUpHelper.resToArr(resSet);
		lookUpHelper.printTable(resSet);
		resSet.first();
			System.out.print("\nInsert index to edit employee\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			if(inputString.equalsIgnoreCase("Back"))  break;
			
			if (includes == true) {
				tempArr = lookUpHelper.resToArr(resSet);
				tempID2 = tempArr.get(x - 1);
				
				editEmployee(resSet);
			}
		
			
		}
	}

	private void chooseEmployeeManager(ResultSet resSet) throws SQLException {

		resSet = stmt.executeQuery(Const.FIRST_LAST_WHERE_MAN + tempID);
		boolean includes = false;
		
		int x = 0;

		while (true) {
		resSet = stmt3.executeQuery(Const.FIRST_LAST_WHERE_MAN + tempID);
		tempArr = lookUpHelper.resToArr(resSet);
		lookUpHelper.printTable(resSet);
		resSet.first();
			System.out.print("\nInsert index to edit employee\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			if (lookUpHelper.isInt(inputString) == true) {
				x = Integer.parseInt(inputString);
				for (int i = 1; i <= tempArr.size(); i++) {
					if (i == x) {
						includes = true;
					}
				}
			}
			if(inputString.equalsIgnoreCase("Back"))  break;
			
			if (includes == true) {
				tempArr = lookUpHelper.resToArr(resSet);
				tempID2 = tempArr.get(x - 1);
				
				editEmployee(resSet);
			}
		
			
		}
	}
	
	private void editEmployee(ResultSet resSet) throws SQLException {
		String updateStr = "";
		if (tempID2.isEmpty() == true) tempID2 = tempID; 
		resSet = stmt.executeQuery(Const.ALL_ABOUT_EMPLOYEE + tempID2 + "'");
		tempArr = lookUpHelper.resToArr(resSet);
		boolean includes = false;
		
		int toInt = 0;
		
		while (true) {
		resSet = stmt3.executeQuery(Const.ALL_ABOUT_EMPLOYEE + tempID2 + "'");
		lookUpHelper.printTableFull(resSet);
		resSet.first();
			System.out.print("\nInsert index to edit details\n"
					+ "or \"Back\" to go back: ");
			inputString = inputScanner.next();
			if (lookUpHelper.isInt(inputString) == true) {
				toInt = Integer.parseInt(inputString);
				for (int i = 1; i <= 11; i++) {
					if (i == toInt) {
						includes = true;
					}
				}
			}
			// Checks if want to go back and if number is a index
			if(inputString.equalsIgnoreCase("Back"))  break;
			if (toInt < 0 || toInt > 11){
				continue; 
			}
			
			System.out.print("\nInsert new value: ");
			updateStr = inputScanner.next();
		
			switch (toInt) {
            case 1:  stmt2.execute(Const.UPDATE_EMPLOYEE_ID + updateStr + 
            		Const.UPDATE_EMPLOYEE_ID2 + tempID2 + "'");
                     break;
                     
            case 2:  stmt2.execute("UPDATE employees SET first_name='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
                     
            case 3:  stmt2.execute("UPDATE employees SET last_name='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 4:  stmt2.execute("UPDATE employees SET email='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 5:  stmt2.execute("UPDATE employees SET phone_number='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 6:  stmt2.execute("UPDATE employees SET hire_date='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 7:  stmt2.execute("UPDATE employees SET job_id='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 8:  stmt2.execute("UPDATE employees SET salary='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 9:  stmt2.execute("UPDATE employees SET commission_pct='" 
                     + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                     break;
            case 10: stmt2.execute("UPDATE employees SET manager_id='" 
                    + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                    break;
            case 11: stmt2.execute("UPDATE employees SET department_id='" 
                    + updateStr + "' WHERE employee_id='" + tempID2 + "'");
                    break;
                     }
			}
	}

	public void closeConnection() {
		try {
			if (resSet != null)  resSet.close();
			if (stmt != null)  stmt.close();
			if (stmt2 != null)  stmt2.close();
			if (stmt3 != null)  stmt3.close();
			
			if (connection != null)  connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
