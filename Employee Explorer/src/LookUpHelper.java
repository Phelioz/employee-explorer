import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class LookUpHelper {

	public LookUpHelper(){
		
	}
	
	public boolean isInt(String str){
		boolean isInt = true;
		char charArr[] = str.toCharArray();
		for (char c : charArr) {
			if (!Character.isDigit(c)) {
				isInt = false;
			}
		}
		
		return isInt;
	}

	public void printTable(ResultSet resSet) throws SQLException{
		try {
			int columns = resSet.getMetaData().getColumnCount();
			int count = 1;
			while (resSet.next()) {
				
				for (int i = 2; i <= columns; i++) {
					if (i == 2 && columns == 2) {
						System.out.print(count + ". " + resSet.getString(i) + "\n");
					}
					
					else if (i == 2) {
						System.out.print(count + ". " + resSet.getString(i) + " ");
					}
					else if (i == columns) {
						System.out.print(resSet.getString(i) + "\n");
						
					}
					else System.out.print(resSet.getString(i) + " ");
				}
				count++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resSet.beforeFirst();
		System.out.println("");
	}

	public void printTableFull(ResultSet resSet) throws SQLException{
		try {
			int columns = resSet.getMetaData().getColumnCount();
			int count = 1;
			while (resSet.next()) {
				
				for (int i = 1; i <= columns; i++) {
					if (i == 1) {
						System.out.print(count + ". " + resSet.getString(i) + "\n");
					}
					else if (i == columns) {
						System.out.print(count + ". " + resSet.getString(i) + "\n");
					}
					else System.out.print(count + ". " + resSet.getString(i) + "\n");
					count++;
				}
				//count++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resSet.beforeFirst();
		System.out.println("");
	}

	public ArrayList<String> resToArr(ResultSet resSet) throws SQLException{
		resSet.beforeFirst();
		ArrayList<String> tempArr = new ArrayList<String>();
		
		try {
			int columns = resSet.getMetaData().getColumnCount();
			resSet.beforeFirst();
			while (resSet.next()) {
				tempArr.add(resSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resSet.beforeFirst();
		return tempArr;
	}
}
